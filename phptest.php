<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    
    require 'admin_backend/data/config.php';

    $item_quantities = array(1,2,3);
    $item_ids = array(4,5,6);
    $section_ids = array(7,8,9);

    $run = $conn->prepare("INSERT INTO room_entries(moving_id, section_id, item_id, item_qty) VALUES((SELECT MAX(moving_id) FROM moving_entries), ?, ?, ?)");
    foreach($item_quantities as $key => $value) {
        $run->bind_param("iii", $section_ids[$key], $item_ids[$key], $value);
        //echo $value . PHP_EOL . $item_ids[$key] . PHP_EOL . $section_ids[$key];
        $run->execute();
    }
    echo 'Successful';    

?>