<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Meta -->
    <meta charset="UTF-8">
    <meta name="author" content="diggys.com.ng">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Diggs.com.ng - Office &amp; Home Moving Company</title>

    <meta name="keywords"
        content="moving service, moving company, relocation service, apartment movers in lagos, relocation companies in lagos, moving companies in abuja, movers and packers in abuja, moving trucks in lagos, moving van rental in lagos, relocation services in lagos, house moving companies in nigeria, fast moving consumer goods companies in nigeria">

    <meta name="description"
        content="diggysmoving.com offers relocation service for office & home moves in Lagos. Our team of house movers and packers will help you with a swift, fast & efficient move">

    <meta itemprop="url" content="https://www.diggysmoving.com" />
    <meta name="author" content="diggysmoving.com" />
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@diggy_ng">
    <meta name="twitter:creator" content="@diggy_ng">

    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Organization",
        "name": "diggys.com.ng",
        "url": "http://www.diggysmoving.com",
    }
    </script>

    <!-- Favicon -->
    <link rel="canonical" href="https://www.diggysmoving.com/" />
    <link rel="icon" type="image/x-icon" href="/assets/images/favcon.png" />
    <link rel="apple-touch-icon" type="image/png" href="/assets/images/favcon.png" />
    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <!-- CSS Links -->
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/plugins.min.css">
    <link rel="stylesheet" href="/assets/css/nexmenu.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="/assets/css/responsive.css">
    <!-- Modernizr JS -->
    <script src="assets/js/modernizr-2.8.3.min.js"></script>

</head>

<body>
    <!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.
</p>
<![endif]-->
    <!-- Preloader -->
    <div class="spinner-area" id="preloader">
        <div class="spinner"></div>
    </div>
    <!-- ./Preloader -->
    <nav class="desk-nav" id="nav">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div id="brand">
                        <a href="https://diggysmoving.com">
                            <img src="/assets/images/diggywhite.png" alt="logo">
                        </a>
                    </div>
                    <div class="nex-menu">
                        <ul class="navbar-menu">
                            <li><a href="/">HOME</a></li>
                            <li><a href="/get-free-quotes.php">GET FREE QUOTES</a></li>
                            <li><a href="#service">SERVICES</a></li>
                            <li><a href="#move">HOW WE MOVE</a></li>
                            <li><a href="https://diggysmoving.com/blog">BLOG</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <!-- ================================Banner Area================ -->
    <div class="banner-area banner-gradient" id="banner">
        <div class="container">
            <div class="hero-content">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-6">
                        <form action="/get-free-quotes" class="banner-text menu-space" id="quote-form">
                            <h3>STARTING AT N49,999 PER MOVE</h3>
                            <div class="select-option">
                                <select class="basic">
                                    <option value="">WHAT ARE YOU MOVING?</option>
                                    <option value="house">HOUSEHOLD ITEMS</option>
                                    <option value="packaged">PACKAGED ITEMS</option>
                                    <option value="office">OFFICE ITEMS</option>
                                </select>
                            </div>
                            <div class="moving-btn-area">
                                <div class="form">
                                    <div class="single-form">
                                        <input name="from" type="text" placeholder="MOVING FROM" value="">
                                    </div>
                                    <div class="single-form">
                                        <input name="to" type="text" placeholder="MOVING TO" value="">
                                    </div>
                                    <div class="get-btn">
                                        <a href="/get-free-quotes.php" id="quote-btn">GET FREE QUOTES NOW</a>
                                    </div>
                                </div>
                            </div>
                            <h6>Or Call</h6>
                            <div class="view-number">
                                <a href="tel:+234-906-851-9438">
                                    <input type="text" name="lname" placeholder="0906-851 ****" disabled>
                                    <button type="button">View number</button>
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="feature-image">
                    <img id="home_feature_arrows" src="/assets/images/arrows.svg" alt="arrows">
                    <img id="home_feature_main" src="/assets/images/process.png" alt="banner-illustration">
                </div>
            </div>
        </div>
    </div>
    <!-- ===========================Moving Area====================== -->
    <div class="moving-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="moving-inner">
                        <div class="moving-left">
                            <div class="moving-left-inner">
                                <div class="company-logo">
                                    <img src="/assets/images/company-logo.png" alt="logo">
                                </div>
                                <h3>#1 MOVING COMPANY IN NIGERIA</h3>
                                <h4>#1 rated One of the Best Moving Company in Nigeria</h4>
                                <p>Whether you're moving across the street or across a cer-
                                    tain region, Diggy.com.ng has got you. Our experienced
                                    team of movers take pride in arranging a seamless move
                                    for your household and business, making it fast and effi-
                                    cient. Our mission is to make your move stress free for you.
                                </p>
                            </div>
                        </div>
                        <div class="moving-right">
                            <img src="/assets/images/courier2e.jpeg" alt="img">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ==============================Counter Area===================== -->
    <div class="counter-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="single-counter">
                        <div class="single-counter-img">
                            <img src="/assets/images/g110.png" alt="img">
                        </div>
                        <div class="single-counter-text">
                            <h1>2,000</h1>
                            <h5>Clients trust in us</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-counter">
                        <div class="single-counter-img">
                            <img src="/assets/images/g32.png" alt="img">
                        </div>
                        <div class="single-counter-text">
                            <h1>200,000</h1>
                            <h5>KM moved</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-counter">
                        <div class="single-counter-img">
                            <img src="/assets/images/g62.png" alt="img">
                        </div>
                        <div class="single-counter-text">
                            <h1>5,000</h1>
                            <h5>Boxes</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-counter">
                        <div class="single-counter-img">
                            <img src="/assets/images/g12.png" alt="img">
                        </div>
                        <div class="single-counter-text">
                            <h1>50,000</h1>
                            <h5>Tons of goods</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="team-area testimonial-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="team-inner-area">
                        <div class="test-title title text-center">
                            <h2>TESTIMONIALS</h2>
                            <h6>Read what our happy and satisfied clients have to say</h6>
                        </div>
                        <!-- ===========================Slider Area====================== -->
                        <div class="test-slider-area">
                            <div class="logo-slider-area-inner ">
                                <div class="test-slider slider text-center">
                                    <div class="test-slider-inner">
                                        <h2>MRS NORA BISONG</h2>
                                        <div class="star">
                                            <span><img src="/assets/images/star.png" alt="star"></span>
                                            <span><img src="/assets/images/star.png" alt="star"></span>
                                            <span><img src="/assets/images/star.png" alt="star"></span>
                                            <span><img src="/assets/images/star.png" alt="star"></span>
                                        </div>
                                        <p>Their service is just flawless. Can't recommend them less. Kind regards.</p>
                                    </div>
                                    <div class="test-slider-inner">
                                        <h2>AKINDELE JOSEPH</h2>
                                        <div class="star">
                                            <span><img src="/assets/images/star.png" alt="star"></span>
                                            <span><img src="/assets/images/star.png" alt="star"></span>
                                            <span><img src="/assets/images/star.png" alt="star"></span>
                                            <span><img src="/assets/images/star.png" alt="star"></span>
                                        </div>
                                        <p>I felt the relief and really refreshed
                                            after using their service due to the stress they took away from me.
                                            Diggysmoving all the way.
                                        </p>
                                    </div>
                                    <div class="test-slider-inner">
                                        <h2>MR CHINONSO DIKE</h2>
                                        <div class="star">
                                            <span><img src="/assets/images/star.png" alt="star"></span>
                                            <span><img src="/assets/images/star.png" alt="star"></span>
                                            <span><img src="/assets/images/star.png" alt="star"></span>
                                            <span><img src="/assets/images/star.png" alt="star"></span>
                                        </div>
                                        <p>I have always thought that a moving service wold not be possible.
                                            Diggysmoving changed that thought.
                                        </p>
                                    </div>
                                    <div class="test-slider-inner">
                                        <h2>JOHN MADUAKO JR</h2>
                                        <div class="star">
                                            <span><img src="/assets/images/star.png" alt="star"></span>
                                            <span><img src="/assets/images/star.png" alt="star"></span>
                                            <span><img src="/assets/images/star.png" alt="star"></span>
                                            <span><img src="/assets/images/star.png" alt="star"></span>
                                        </div>
                                        <p>Diggy is by far one
                                            of the best services I have ever
                                            paid for. With just 50,000
                                            Naira, i was able to move into
                                            my new home without any loss
                                            or damage to my
                                            belongings.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- =================================Service Area========================= -->
    <div class="service-area" id="service">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="service-title title text-center">
                        <h2>OUR SERVICES</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="brand-area-2 posr" id="move">
        <div class="container-fluid custom-container">
            <div class="row">
                <div class="col-md-6">
                    <div class="contact-brand-right">
                        <div class="service-text">
                            <h2>RESIDENTIAL/LOCAL MOVE</h2>
                            <p>Any relocation within a state is a local move. Whether we have to climb
                                the stairs or not, requires care and professionalism. Diggy offers a full
                                range of local moving services within Lagos including full packing and
                                set up services. We also offer professional packing for fragile items like
                                fine art, antiques, marble and glassware. No matter what your needs are,
                                diggy.com.ng can provide a level of customer service and expertise
                                unmatched by other apartment movers in Lagos.
                            </p>
                            <p>Whether you're moving from the 6th to 12th floor or from Alimosho to
                                Lekki in Lagos, diggy.com.ng is the best way to get there. Our fast
                                moving service has earned diggy hundreds of satisfied customers and
                                the reputation as the local moving company to trust in Lagos.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="brand-area posr blue-bg">
        <div class="container-fluid custom-container">
            <div class="row">
                <div class="col-md-6 offset-md-6">
                    <div class="contact-brand-right">
                        <div class="service-text">
                            <h2>INTERSTATE/LONG DISTANCE MOVE</h2>
                            <p>Whether you are moving/relocating from Lagos to Abuja, or from any
                                state within Nigeria, Diggy got you. Interstate moving or a long
                                distance move requires proper planning to make it a smooth and
                                stress-free experience. Starting with the moving quote process, we
                                partner with you to gather all the information you need to make the
                                decision we need to complete your long distance move on budget
                                and on time. You can start the process by requesting a free long dis-
                                tance moving quote from Diggy.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="brand-area-3 posr mr-100">
        <div class="container-fluid custom-container">
            <div class="row">
                <div class="col-md-6">
                    <div class="contact-brand-right ">
                        <div class="service-text">
                            <h2>COMMERCIAL/OFFICE MOVE</h2>
                            <p>You have so many responsibilities at work from managing employees to
                                servicing clients. That's why we take over when it's time for your office
                                move so you can focus on your current business without missing a beat.
                                Whether you're moving your business along with your home or you're
                                expanding your business to another location, Diggy is the pre-
                                mier commercial moving company for companies of all sizes.
                            </p>
                            <p>Years of experience has made us the expert commercial movers when it
                                comes to office relocation. During your commercial move, transporting
                                your office's inventory and excess stock isn't a problem. We follow a care-
                                fully crafted system of packing, transporting, loading and offloading of
                                your business items ensuring safety, efficiency and speed of transfer. We
                                understand every aspect of commercial moving which allows us avoid
                                unnecessary costs that most inexperienced commercial movers incur on
                                their clients. Our commercial movers make moving a great experience.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="brand-area-4  posr blue-bg">
        <div class="container-fluid custom-container">
            <div class="row">
                <div class="col-md-6 offset-md-6">
                    <div class="contact-brand-right">
                        <div class="service-text">
                            <h2>REAL ESTATE</h2>
                            <p>
                                It’s not always possible to find enough time to do a deep house
                                clean. We lead busy lives, often balancing family and one or two jobs.
                                That’s where Diggy Comes in. We’ve worked with several cus-
                                tomers to give their homes a professional level of service. Whether
                                it’s one room or an entire house, we’re happy to take on any cleaning
                                job. We deliver the highest standard.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ================================Started Area==================== -->
    <div class="started-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="started-text title text-center">
                        <h2>GET STARTED TODAY!</h2>
                        <h6>Let us know more about your move</h6>
                        <a href="/get-free-quotes.php">GET QUOTE</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ================================Touch Area==================== -->
    <div class="touch-area blue-bg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="touch-inner">
                        <div class="touch-inner-left">
                            <p>
                                Diggys moving company limited is conceived as a multifaceted service outfit
                                that offers cost beneficial solutions for individuals, corporate , Government
                                and Non-Government institutions , multinational companies etc.
                            </p>

                            <p>
                                We are commuted to offering our clients the highest level of service
                                delivery and making every move decent , stress free and cost effective.
                            </p>

                            <p>
                                Our property section is built to help you find properties that fits both
                                your budget and lifestyle.
                            </p>

                            <p>
                                With our team of highly skilled professionals, we ensure that the whole
                                value chain of our service offering does not just meet our client's desire
                                but exceed them . We employ best industry and global practices in every move,
                                cleaning or associated service we offer.
                            </p>
                        </div>
                        <div class="touch-inner-right">
                            <h2>GET IN TOUCH WITH US</h2>
                            <img src="/assets/images/diggywhite.png" alt="logo">
                            <h4>Suite D 57, Efab Mall Extension, Area 11, Abuja.</h4>
                            <a href="mailto:info@diggy.com.ng">Email us: info@diggysmoving.com</a>
                            <div class="social-icon">
                                <a href="https://web.facebook.com/Diggys-Moving-108330037404153/"><img
                                        src="/assets/images/fb.svg" alt="fb"></a>
                                <!-- <a href="https://twitter.com/#"><img src="/assets/images/twt.svg" alt="twt"></a> -->
                                <a href="https://www.instagram.com/diggysmoving/"><img src="/assets/images/inst.png"
                                        alt="ins"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery -->
        <script data-cfasync="false" src="/assets/js/email-decode.min.js"></script>
        <script src="/assets/js/jquery-1.11.2.min.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
        <script src="/assets/js/plugins.min.js"></script>
        <script src="/assets/js/nexmenu.js"></script>
        <script src='/assets/js/jquery-ui.min.js'></script>
        <script src="/assets/js/main.js"></script>
        <script>
        $(function() {
            $("#quote-btn").click(function() {
                $("#quote-form").submit();
            })
        })
        </script>
</body>

</html>