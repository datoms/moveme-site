<div id="form_list" class="col-lg-12">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">

        <div class="carousel-inner" role="listbox">

            <div class="item finish active">
                <div class="col-lg-5 col-md-5 col-sm-12 text-cont">
                    <h2><strong>YOUR MOVE IS NOW BEING PROCESSED</strong></h2>
                    <h4><strong>Dear Nelson Okeyson</strong></h4>
                    <p>Thank you for your time, we are working on your request and will get back to you shortly.</p>
                    <a href="https://moveme.com.ng/services" class="btn secondry">learn more</a>
                </div>
                <div class="col-lg-7 col-md-7 hidden-sm hidden-xs img-cont">
                    <img src="https://moveme.com.ng/images/new/move_me_truck.png?v1" alt="MoveMe truck" />
                </div>

            </div>

        </div>

        <!-- Controls -->

    </div>

</div>