<?php

session_start();
require 'config.php';

$user = $_SESSION['username'];

$stmt = $conn->prepare("SELECT * FROM users WHERE username = ?");
$stmt->bind_param("s", $user);
$stmt->execute();
$result = $stmt->get_result();
$row = $result->fetch_array(MYSQLI_ASSOC);

$username = $row['username'];
$email = $row['email'];
$name = $row['full_name'];
$created = $row['created'];

if(!isset($user)) {
    header("Location: /admin_backend/register.php");
}