<?php
/*
	Sample Processing of Register form via ajax
	Page: register.php
*/
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

require 'config.php';

//define("SALT", ",&43E{R,$!R7[<e2RnF$^sW!<+|v5<ue<9IAC%(h|0j&n;GS^217R,E:f/%[,j");

if(isset($_POST['action']) && $_POST['action'] == 'register') {


	// Fields Submitted
	$name       	= check_input($_POST['name']);
	$uname   		= check_input($_POST['uname']);
	$email      	= check_input($_POST['email']);
	$pass   		= check_input($_POST['pass']);
	$cpass   		= check_input($_POST['cpass']);
	//$pass			= hash('sha256', SALT.$pass);
	//$cpass 			= hash('sha256', SALT.$cpass);
	$pass			= sha1($pass);
	$cpass			= sha1($cpass);
	$created		= date('Y-m-d');

	if($pass != $cpass) {
		echo 'Passwords do not match';
		exit();
	} else {
		$sql = $conn->prepare("SELECT username,email FROM users WHERE username=? OR email=?");
		$sql->bind_param("ss", $uname,$email);
		$sql->execute();
		$result = $sql->get_result();
		$row  = $result->fetch_array(MYSQLI_ASSOC);

		if($row['username'] == $uname) {
			echo "Username already exists";
		} elseif($row['email']  == $email) {
			echo "Email is already registered";
		} else {
			$push = $conn->prepare("INSERT INTO users (full_name, username, email, user_pass, created) VALUES (?, ?, ?, ?, ?)");
			$push->bind_param("sssss", $name, $uname, $email, $pass, $created);
			if($push->execute()) {
				echo "Registered Successfully. You can now login";
			} else {
				trigger_error("there was an error....".$conn->error, E_USER_WARNING);
			}

		}
	}

}

if(isset($_POST['action']) && $_POST['action'] == 'login') {
	session_start();

	$username = $_POST['username'];
	$password = sha1($_POST['password']);

	$stmt_login = $conn->prepare("SELECT * FROM users WHERE username = ? AND user_pass = ?");
	$stmt_login->bind_param("ss", $username, $password);
	$stmt_login->execute();
	$user = $stmt_login->fetch();

	if($user != null) {
		$_SESSION['username'] = $username;
		echo 'ok';

		if(!empty($_POST['rem'])) {
			setcookie("username", $_POST['username'], time()+(10*365*24*60*60));
			setcookie("password", $_POST['password'], time()+(10*365*24*60*60));
		} else {
			if(isset($_COOKIE['username'])) {
				setcookie("username", "");
			}
			if(isset($_COOKIE['password'])) {
				setcookie("password", "");
			}
		}
	} else {
		echo 'Login failed! Check your username and password';
	}
}

function check_input($data) {
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}