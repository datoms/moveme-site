<?php
    require '../data/config.php';

    $delete_id = $_GET['delete'];

    $del_from_room_entries = "DELETE FROM room_entries WHERE moving_id = ?";
    $del_from_moving_entries = "DELETE FROM moving_entries WHERE moving_id = ?";

    $del_prepare_room = $conn->prepare($del_from_room_entries);
    $del_prepare_room->bind_param('i', $delete_id);

    $del_prepare_move = $conn->prepare($del_from_moving_entries);
    $del_prepare_move->bind_param('i', $delete_id);

    if($del_prepare_room->execute() && $del_prepare_move->execute()) {
        header("Location: ../entries.php");
    } else {
    trigger_error("there was an error....".$conn->error, E_USER_WARNING);
    }