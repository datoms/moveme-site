<br />
<!-- Footer -->
<footer class="main">

    &copy; 2020 <strong>Diggy's</strong> Admin Dashboard by <a href="https://webedge.com.ng"
        target="_blank"><strong>WebEdge</strong></a>

</footer>
</div>

</div>

<!-- Imported styles on this page -->
<link rel="stylesheet" href="assets/js/jvectormap/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet" href="assets/js/rickshaw/rickshaw.min.css">

<!-- Imported styles on this entries -->
<link rel="stylesheet" href="assets/js/datatables/datatables.css">
<link rel="stylesheet" href="assets/js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="assets/js/select2/select2.css">


<!-- Bottom scripts (common) -->

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>


<script src=" assets/js/gsap/TweenMax.min.js"></script>
<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<!-- <script src="assets/js/bootstrap.js"></script> -->
<script src="assets/js/joinable.js"></script>
<script src="assets/js/resizeable.js"></script>
<script src="assets/js/neon-api.js"></script>
<script src="assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>


<!-- Imported scripts on this page -->
<script src="assets/js/raphael-min.js"></script>
<script src="assets/js/morris.min.js"></script>

<!-- JavaScripts initializations and stuff -->
<script src="assets/js/neon-custom.js"></script>

<!-- Imported scripts on edit page -->
<script src="assets/js/jquery.inputmask.bundle.js"></script>

<!-- Imported scripts on entries page -->
<script src="assets/js/datatables/datatables.js"></script>
<script src="assets/js/select2/select2.min.js"></script>
<script src="assets/js/delete.js"></script>
<!-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script> -->


<!-- Imported scripts on settings page -->
<script src="assets/js/jquery.validate.min.js"></script>


<!-- Demo Settings -->
<script src="assets/js/neon-demo.js"></script>

</body>

</html>