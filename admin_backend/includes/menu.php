<div class="sidebar-menu">

    <div class="sidebar-menu-inner">

        <header class="logo-env">

            <!-- logo -->
            <div class="logo">
                <a href="dashboard.php">
                    <img src="assets/images/diggywhite.png" width="120" alt="" />
                </a>
            </div>

            <!-- logo collapse icon -->
            <div class="sidebar-collapse">
                <a href="#" class="sidebar-collapse-icon">
                    <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                    <i class="entypo-menu"></i>
                </a>
            </div>


            <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
            <div class="sidebar-mobile-menu visible-xs">
                <a href="#" class="with-animation">
                    <!-- add class "with-animation" to support animation -->
                    <i class="entypo-menu"></i>
                </a>
            </div>

        </header>


        <ul id="main-menu" class="main-menu">
            <!-- add class "multiple-expanded" to allow multiple submenus to open -->
            <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
            <li <?php echo $page == 'home' ? 'class="active"' : ''; ?>>
                <a href="dashboard.php">
                    <i class="entypo-gauge"></i>`
                    <span class="title">Dashboard</span>
                </a>
            </li>
            <li <?php echo $page == 'entries' ? 'class="active"' : ''; ?>>
                <a href="entries.php">
                    <i class="entypo-window"></i>
                    <span class="title">Entries</span>
                </a>
            </li>
            <!-- <li <?php //echo $page == 'edit-entry' ? 'class="active"' : ''; ?>>
                <a href="edit-entry.php">
                    <i class="entypo-doc-text"></i>
                    <span class="title">Edit</span>
                </a>
            </li> -->
            <!-- <li>
					<a href="notes.php">
					<i class="entypo-doc"></i>
						<span class="title">Notes</span>
					</a>
				</li> -->
            <!-- <li <?php //echo $page == 'profile' ? 'class="active"' : ''; ?>>
                <a href="profile.php">
                    <i class="entypo-user"></i>
                    <span class="title">Profile</span>
                </a>
            </li>
            <li>
                <a href="settings.php">
                    <i class="entypo-cog"></i>
                    <span class="title">Settings</span>
                </a>
            </li> -->
        </ul>

    </div>

</div>