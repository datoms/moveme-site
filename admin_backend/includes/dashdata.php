<?php
    $count_entries = $conn->prepare('SELECT COUNT(moving_id) FROM moving_entries');
	$count_entries->execute();
	$result = $count_entries->get_result()->fetch_row();
	$display = $result[0];

	$count_users = $conn->prepare('SELECT COUNT(id) FROM users');
	$count_users->execute();
	$get_users = $count_users->get_result()->fetch_row();
	$show_users = $get_users[0];