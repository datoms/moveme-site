<?php 
	$page = 'edit-entry';
	include("includes/header.php"); 
?>
		
					<ol class="breadcrumb bc-3" >
								<li>
						<a href="index.html"><i class="fa-home"></i>Home</a>
					</li>
							<li>
		
									<a href="forms-main.html">Forms</a>
							</li>
						<li class="active">
		
									<strong>Input Masks</strong>
							</li>
							</ol>
					
		<h2>Input Masks</h2>
		<br />
		
		<div class="panel panel-primary" data-collapsed="0">
		
			<div class="panel-heading">
				<div class="panel-title">
					Fields are not required, but you cannot type something different than the applied rule.
				</div>
				
				<div class="panel-options">
					<a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
					<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
					<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
					<a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
				</div>
			</div>
			<div class="panel-body">
				
				<form role="form" class="form-horizontal form-groups-bordered">
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Numeric Mask</label>
						
						<div class="col-sm-5">
							<input type="text" class="form-control" data-mask="999" placeholder="Three Numbers" />
							<br />
							<input type="text" class="form-control" data-mask="9999" data-numeric="true" data-numeric-align="right" placeholder="Four Numbers Right" />
							<br />
							<input type="text" class="form-control" data-mask="decimal" placeholder="Decimal" />
							<br />
							<input type="text" class="form-control" data-mask="fdecimal" placeholder="Formatted Decimal" data-dec="," data-rad="." maxlength="10" />
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Date</label>
						
						<div class="col-sm-5">
							<input type="text" class="form-control" data-mask="date" />
							
							<br />
							
							<input type="text" class="form-control" data-mask="d/m/y" />
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Date Time</label>
						
						<div class="col-sm-5">
							<input type="text" class="form-control" data-mask="datetime" />
							
							<br />
							
							<input type="text" class="form-control" data-mask="datetime12" />
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Email</label>
						
						<div class="col-sm-5">
							<input type="text" class="form-control" data-mask="email" />
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Phone</label>
						
						<div class="col-sm-5">
							<input type="text" class="form-control" data-mask="phone" />
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Currency</label>
						
						<div class="col-sm-5">
							<input type="text" class="form-control" data-mask="currency" data-sign="€" />
							<br />
							<input type="text" class="form-control" data-mask="rcurrency" data-sign="$" />
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Custom Formatting</label>
						
						<div class="col-sm-5">
							<input type="text" class="form-control" data-mask="@\w+" data-is-regex="true" placeholder="@username" />
							<br />
							<input type="text" class="form-control" data-mask="09-999-AA" placeholder="Kosovo Plate Format - 09-999-AA" />
							<br />
							<input type="text" class="form-control" data-mask="[A-Z][a-z][A-Z][a-z][A-Z][a-z][A-Z][a-z][A-Z][a-z]" data-is-regex="true" placeholder="Advanced Regex - One upper case, one lower case." />
						</div>
					</div>
					
				</form>
				
			</div>
			
		</div>
<?php include("includes/footer.php"); ?>