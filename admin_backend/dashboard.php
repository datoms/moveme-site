<?php 
	$page = 'home';
	include("includes/header.php");
	include("includes/dashdata.php");
?>

<script type="text/javascript">
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
</script>


<div class="row">

    <div class="container-fluid">
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h3 class="text-center display-6">Welcome</h3>
                <h2 class="text-center display-2 text-uppercase"><?= $name ?></h2>
                <h3 class="text-center display-5">Email: <?= $email ?></h3>
                <h3 class="text-center display-5">Registered on: <?= $created ?></h3>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-xs-6">

        <div class="tile-stats tile-green">
            <div class="icon"><i class="entypo-chart-bar"></i></div>
            <div class="num" data-start="0" data-end="<?= $show_users; ?>" data-postfix="" data-duration="1500"
                data-delay="600">0</div>

            <h3>Users</h3>
        </div>

    </div>

    <div class="clear visible-xs"></div>

    <div class="col-sm-6 col-xs-6">

        <div class="tile-stats tile-aqua">
            <div class="icon"><i class="entypo-mail"></i></div>
            <div class="num" data-start="0" data-end="<?= $display; ?>" data-postfix="" data-duration="1500"
                data-delay="1200">0</div>

            <h3>Entries</h3>
        </div>

    </div>

    <!-- <div class="col-sm-4 col-xs-6">

        <div class="tile-stats tile-blue">
            <div class="icon"><i class="entypo-rss"></i></div>
            <div class="num" data-start="0" data-end="52" data-postfix="" data-duration="1500" data-delay="1800">0</div>

            <h3>Testimonies</h3>
        </div>

    </div> -->
</div>

<br />

<?php include("includes/footer.php"); ?>