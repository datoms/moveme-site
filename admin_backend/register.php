<?php
	session_start();
	if(isset($_SESSION['username'])) {
		header("Location: dashboard.php");
	}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Diggys Moving Register" />
    <meta name="author" content="" />

    <link rel="icon" href="assets/images/favcon.png">

    <title>Diggys Moving | Register</title>

    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

    <style type="text/css">
    #alert,
    #register-box,
    #forgot-box {
        display: none;
    }
    </style>

</head>

<body data-url="http://diggysmoving.dev" class="bg-dark">

    <div class="container mt-4">
        <div class="row">
            <div class="col-lg-4 offset-lg-4" id="alert">
                <div class="alert alert-success">
                    <strong id="result"></strong>
                </div>
            </div>
        </div>

        <!-- Login Form -->
        <div class="row">
            <div class="col-lg-4 offset-lg-4 rounded bg-light" id="login-box">
                <h2 class="text-center mt-2">Login</h2>
                <form action="" method="post" role="form" class="p-2" id="login-frm">
                    <div class="form-group">
                        <input type="text" name="username" class="form-control" placeholder="Username" required
                            value="<?php if(isset($_COOKIE['username'])){ echo $_COOKIE['username']; } ?>">
                    </div>

                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Password"
                            value="<?php if(isset($_COOKIE['password'])){ echo $_COOKIE['password']; } ?>" required>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="rem" class="custom-control-input" id="customCheck"
                                <?php if(isset($_COOKIE['username'])){ ?> checked <?php } ?>>
                            <label for="customCheck" class="custom-control-label">Remember Me</label>
                            <a href="#" id="forgot-btn" class="float-right" style="display:none">Forgot Password?</a>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="login" value="Login" id="login" class="btn btn-primary btn-block">
                    </div>
                    <div class="form-group" style="display:none">
                        <p class="text-center">New User? <a href="#" id="register-btn">Register Here</a></p>
                    </div>
                </form>
            </div>
        </div>

        <!-- Registration Form -->

        <div class="row">
            <div class="col-lg-4 offset-lg-4 rounded bg-light" id="register-box">
                <h2 class="text-center mt-2">Register</h2>
                <form action="" method="post" role="form" class="p-2" id="register-frm">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" placeholder="Full Name" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="uname" class="form-control" placeholder="Username" required>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="Email" required>
                    </div>
                    <div class="form-group">
                        <input type="password" name="pass" id="pass" class="form-control" placeholder="Password"
                            required>
                    </div>
                    <div class="form-group">
                        <input type="password" name="cpass" id="cpass" class="form-control"
                            placeholder="Confirm Password" required>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="rem" class="custom-control-input" id="customCheck2">
                            <label for="customCheck2" class="custom-control-label">I agree to <a href="#">Terms &
                                    Conditions</a></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="register" value="Register" class="btn btn-primary btn-block"
                            id="register">
                    </div>
                    <div class="form-group">
                        <p class="text-center">Already Registered? <a href="#" id="login-btn">Login Here</a></p>
                    </div>
                </form>
            </div>
        </div>

        <!-- Forget Password -->
        <div class="row">
            <div class="col-lg-4 offset-lg-4 rounded bg-light" id="forgot-box">
                <h2 class="text-center mt-2">Reset Password</h2>
                <form action="" method="post" role="form" class="p-2" id="forgot-frm">
                    <div class="form-group">
                        <small class="text-muted">
                            To reset your password, enter your email password and we will
                            send reset password instructions to your email.
                        </small>
                    </div>
                    <div class="form-group">
                        <input type="email" name="femail" class="form-control" placeholder="Email" required>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="forgot" value="Reset" id="reset" class="btn btn-primary btn-block">
                    </div>
                    <div class="form-group text-center">
                        <a href="#" id="back-btn">Back</a>
                    </div>
                </form>
            </div>
        </div>

    </div>



    <!-- This is needed when you send requests via Ajax -->
    <script type="text/javascript">
    var baseurl = '';
    </script>


    <!-- Bottom scripts (common) -->
    <script src="http://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
    <script src="assets/js/jquery.validate.min.js"></script>
    <script src="assets/js/register.js"></script>

</body>

</html>