<?php 
	$page = 'entries';
	include("includes/header.php");

	$sql_query = 'SELECT moving_id, full_name, phone, email, from_state, to_state, created FROM moving_entries';
	$prepare_query = $conn->prepare($sql_query);
	$prepare_query->execute();
	$get_entries_result = $prepare_query->get_result()->fetch_all(MYSQLI_ASSOC);
	// echo '<pre>';
	// print_r($get_entries_result);
    // echo '</pre>';
    
    $delete_query = 'DELETE FROM room_entries WHERE '
?>

<ol class="breadcrumb bc-3">
    <li>
        <a href="index.php"><i class="fa-home"></i>Home</a>
    </li>
    <li class="active">
        <strong>Submissions</strong>
    </li>
</ol>

<h3>Quote Submissions</h3>
<br />

<script type="text/javascript">
jQuery(document).ready(function($) {
    var $table3 = jQuery("#table-3");

    var table3 = $table3.DataTable({
        "aLengthMenu": [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ]
    });

    // Initalize Select Dropdown after DataTables is created
    $table3.closest('.dataTables_wrapper').find('select').select2({
        minimumResultsForSearch: -1
    });

    // Setup - add a text input to each footer cell
    $('#table-3 tfoot th').each(function() {
        var title = $('#table-3 thead th').eq($(this).index()).text();
        $(this).html('<input type="text" class="form-control" placeholder="Search ' + title + '" />');
    });

    // Apply the search
    table3.columns().every(function() {
        var that = this;

        $('input', this.footer()).on('keyup change', function() {
            if (that.search() !== this.value) {
                that
                    .search(this.value)
                    .draw();
            }
        });
    });
});
</script>
<br />

<table class="table table-bordered datatable" id="table-3">
    <thead>
        <tr class="replace-inputs">
            <!-- <th>
                <div class="checkbox checkbox-replace">
                    <input type="checkbox" id="chk-1">
                </div>
            </th> -->
            <th>Name</th>
            <th>Phone No</th>
            <th>Email</th>
            <th>From State</th>
            <th>To State</th>
            <th>Created</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php

			foreach ($get_entries_result as $entry_result) {
		?>
        <tr class="odd gradeX">
            <!-- <td>
                <div class="checkbox checkbox-replace">
                    <input type="checkbox" id="chk-1">
                </div>
            </td> -->
            <td><?= $entry_result['full_name'] ?></td>
            <td><?= $entry_result['phone'] ?></td>
            <td><?= $entry_result['email'] ?></td>
            <td><?= $entry_result['from_state'] ?></td>
            <td class="center"><?= $entry_result['to_state'] ?></td>
            <td class="center"><?= $entry_result['created'] ?></td>

            <td class="center">
                <a href="details.php?view=<?= $entry_result['moving_id']; ?>"
                    class="btn btn-info btn-sm btn-icon icon-left">
                    <i class="entypo-eye"></i>
                    View
                </a>
                <!-- <a href="#" class="btn btn-default btn-sm btn-icon icon-left">
                    <i class="entypo-pencil"></i>
                    Edit
                </a> -->

                <a href="includes/delete.php?delete=<?= $entry_result['moving_id']; ?>"
                    class="btn btn-danger btn-sm btn-icon icon-left">
                    <i class="entypo-cancel"></i>
                    Delete
                </a>
            </td>

        </tr>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <!-- <th class="table-footer__remove">&nbsp;</th> -->
            <th>Name</th>
            <th>Phone</th>
            <th>Email</th>
            <th>From State</th>
            <th>To State</th>
            <th>Created</th>
            <th class="table-footer__remove">&nbsp;</th>
        </tr>
    </tfoot>
</table>

<!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
    Launch demo modal
</button> -->

<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Delete Message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="svg-container">
                    <svg class="ft-green-tick" xmlns="http://www.w3.org/2000/svg" height="100" width="100"
                        viewBox="0 0 48 48" aria-hidden="true">
                        <circle class="circle" fill="#5bb543" cx="24" cy="24" r="22" />
                        <path class="tick" fill="none" stroke="#FFF" stroke-width="6" stroke-linecap="round"
                            stroke-linejoin="round" stroke-miterlimit="10" d="M14 27l5.917 4.917L34 17" />
                    </svg>
                    <p class="display-6">Deleted</p>
                </div>
            </div>
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger">Delete</button>
            </div> -->
        </div>

    </div>
</div>

<?php include("includes/footer.php"); ?>