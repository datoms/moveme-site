<?php 
	$page = 'entries';
    include("includes/header.php");
    
    $view = $_GET['view'];
    $living_room_id = 1;
    $bedroom_id = 2;
    $kitchen_id = 3;
    $others_id = 4;

    $sql_query = "SELECT * FROM moving_entries WHERE moving_id = ?";
    $prepare_query = $conn->prepare($sql_query);
    $prepare_query->bind_param('i', $view);
	$prepare_query->execute();
	$get_entries_result = $prepare_query->get_result()->fetch_all(MYSQLI_ASSOC);
	
    
    //Getting the records for rooms

    $living_room = "SELECT item_qty, item_name FROM room_entries r 
                    JOIN items t ON t.section_id = r.section_id AND t.item_id = r.item_id 
                    JOIN moving_entries m ON m.moving_id = r.moving_id 
                    WHERE m.moving_id = ? AND r.section_id = ?";
                    
    $bed_room = "SELECT item_qty, item_name FROM room_entries r 
                    JOIN items t ON t.section_id = r.section_id AND t.item_id = r.item_id 
                    JOIN moving_entries m ON m.moving_id = r.moving_id 
                    WHERE m.moving_id = ? AND r.section_id = ?";

    $kichen = "SELECT item_qty, item_name FROM room_entries r 
                    JOIN items t ON t.section_id = r.section_id AND t.item_id = r.item_id 
                    JOIN moving_entries m ON m.moving_id = r.moving_id 
                    WHERE m.moving_id = ? AND r.section_id = ?";

    $others = "SELECT item_qty, item_name FROM room_entries r 
                    JOIN items t ON t.section_id = r.section_id AND t.item_id = r.item_id 
                    JOIN moving_entries m ON m.moving_id = r.moving_id 
                    WHERE m.moving_id = ? AND r.section_id = ?";
    
    //Preparing statements to get results living room
    $lr_prepare = $conn->prepare($living_room);
    $lr_prepare->bind_param("ii", $view, $living_room_id);
    $lr_prepare->execute();
    $lr_results = $lr_prepare->get_result()->fetch_all(MYSQLI_ASSOC);

    //Preparing statements to get results bedroom
    $br_prepare = $conn->prepare($bed_room);
    $br_prepare->bind_param("ii", $view, $bedroom_id);
    $br_prepare->execute();
    $br_results = $br_prepare->get_result()->fetch_all(MYSQLI_ASSOC);

    //Preparing statements to get results Kitchen
    $kr_prepare = $conn->prepare($kichen);
    $kr_prepare->bind_param("ii", $view, $kitchen_id);
    $kr_prepare->execute();
    $kr_results = $kr_prepare->get_result()->fetch_all(MYSQLI_ASSOC);

    //Preparing statements to get results other rooms
    $others_prepare = $conn->prepare($others);
    $others_prepare->bind_param("ii", $view, $others_id);
    $others_prepare->execute();
    $others_results = $others_prepare->get_result()->fetch_all(MYSQLI_ASSOC);

    // echo '<pre>';
    // print_r($lr_results);
    // echo '</pre>';
    // echo '<br><br>';

    // echo '<pre>';
    // print_r($br_results);
    // echo '</pre>';
    // echo '<br><br>';

    // echo '<pre>';
    // print_r($kr_results);
    // echo '</pre>';
    // echo '<br><br>';
    
    // echo '<pre>';
    // print_r($others_results);
    // echo '</pre>';

?>

<ol class="breadcrumb bc-3">
    <li>
        <a href="dashboard.php"><i class="fa-home"></i>Home</a>
    </li>
    <li class="active">
        <strong>details</strong>
    </li>
</ol>

<h3>Moving details</h3>
<br />

<table class="table table-bordered datatable" id="table-3">
    <thead>
        <tr class="replace-inputs">
            <!-- <th>
                <div class="checkbox checkbox-replace">
                    <input type="checkbox" id="chk-1">
                </div>
            </th> -->
            <th>Label</th>
            <th>Entry</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($get_entries_result as $get_entry) {
        ?>
        <tr class="odd gradeX">
            <!-- <td>
                <div class="checkbox checkbox-replace">
                    <input type="checkbox" id="chk-1">
                </div>
            </td> -->
            <td>Full Name</td>
            <td><?= $get_entry['full_name']; ?></td>
        </tr>
        <tr class="odd gradeX">
            <td>Phone Number</td>
            <td><?= $get_entry['phone']; ?></td>
        </tr>
        <tr class="even gradeX">
            <td>Email</td>
            <td><?= $get_entry['email']; ?></td>
        </tr>
        <tr class="odd gradeX">
            <td>From Address</td>
            <td><?= $get_entry['from_address']; ?></td>
        </tr>
        <tr class="odd gradeX">
            <td>From State</td>
            <td><?= $get_entry['from_state']; ?></td>
        </tr>
        <tr class="odd gradeX">
            <td>To Address</td>
            <td><?= $get_entry['to_address']; ?></td>
        </tr>
        <tr class="odd gradeX">
            <td>To State</td>
            <td><?= $get_entry['to_state']; ?></td>
        </tr>
        <tr class="odd gradeX">
            <td>Apartment (Moving From)</td>
            <td>
                <strong>Type:</strong> &emsp; <?= $get_entry['from_type'];?><br>
                <strong>Beds:</strong> &emsp;<?= $get_entry['from_beds'];?><br>
                <strong>Floor:</strong> &emsp;<?= $get_entry['from_floor'];?>
            </td>
        </tr>
        <tr class="odd gradeX">
            <td>Apartment (Moving to)</td>
            <td>
                <strong>Type:</strong> &emsp; <?= $get_entry['to_type'];?><br>
                <strong>Beds:</strong> &emsp;<?= $get_entry['to_beds'];?><br>
                <strong>Floor:</strong> &emsp;<?= $get_entry['to_floor'];?>
            </td>
        </tr>
        <?php } ?>

        <tr class="odd gradeX">
            <td>Living Room Items</td>
            <td>
                <?php
                foreach ($lr_results as $lr_result) {
            ?>
                <ul>
                    <li><?= $lr_result['item_qty'] . ' ' . $lr_result['item_name']; ?></li>
                </ul>
                <?php } ?>
            </td>

        </tr>

        <tr class="odd gradeX">
            <td>Bedroom Items</td>
            <td>
                <?php
                foreach ($br_results as $br_result) {
                ?>
                <ul>
                    <li><?= $br_result['item_qty'] . ' ' . $br_result['item_name']; ?></li>
                </ul>
                <?php } ?>
            </td>
        </tr>

        <tr class="odd gradeX">
            <td>Kitchen Items</td>
            <td>
                <?php
                    foreach ($kr_results as $kr_result) {
                ?>
                <ul>
                    <li><?= $kr_result['item_qty'] . ' ' . $kr_result['item_name']; ?></li>
                </ul>
                <?php } ?>
            </td>
        </tr>

        <tr class="odd gradeX">
            <td>Other Items</td>
            <td>
                <?php
                    foreach ($others_results as $others_result) {
                ?>
                <ul>
                    <li><?= $others_result['item_qty'] . ' ' . $others_result['item_name']; ?></li>
                </ul>
                <?php } ?>
            </td>
        </tr>


        <?php
        foreach ($get_entries_result as $get_entry) {
        ?>
        <tr class="odd gradeX">
            <td>Additional Items</td>
            <td><?= $get_entry['additional_items']; ?></td>
        </tr>
        <tr class="odd gradeX">
            <td>Services</td>
            <td><?= $get_entry['required_services']; ?></td>
        </tr>
        <tr class="odd gradeX">
            <td>Move Date</td>
            <td><?= $get_entry['move_date']; ?></td>
        </tr>
        <tr class="odd gradeX">
            <td>Referral Code</td>
            <td><?= $get_entry['referral_code']; ?></td>
        </tr>
        <tr class="odd gradeX">
            <td>Special Instructions</td>
            <td><?= $get_entry['special_instruction']; ?></td>
        </tr>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <!-- <th class="table-footer__remove">&nbsp;</th> -->
            <th>Label</th>
            <th>Entry</th>
        </tr>
    </tfoot>
</table>
<div class="col-12 text-right">
    <a href="entries.php" class="btn btn-blue">Back</a>
</div>

<?php include("includes/footer.php"); ?>