$(document).ready(function(){
    $("#forgot-btn").click(function(){
        $("#login-box").hide();
        $("#forgot-box").show();
    });
    $("#back-btn").click(function(){
        $("#forgot-box").hide();
        $("#login-box").show();
    });
    $("#register-btn").click(function(){
        $("#login-box").hide();
        $("#register-box").show();
    });
    $("#login-btn").click(function(){
        $("#register-box").hide();
        $("#login-box").show();
    });
    $("#login-frm").validate();
    $("#register-frm").validate({
        rules: {
            cpass: {
                equalTo: "#pass",
            }
        }
    });
    $("#forgot-frm").validate();

    // Submit form without page refresh for registration section

    $("#register").click(function(e) {
        if(document.getElementById('register-frm').checkValidity()) {
            e.preventDefault();
            $.ajax({
                url: 'data/register-form.php',
                method: 'POST',
                data:$("#register-frm").serialize()+'&action=register',

                success: function(response)
                {	
                    $("#alert").show();
                    $("#result").html(response);
                }
            });
        }
        return true;
    })

    // For login section
    $("#login").click(function(e) {
        if(document.getElementById('login-frm').checkValidity()) {
            e.preventDefault();
            $.ajax({
                url: 'data/register-form.php',
                method: 'POST',
                data:$("#login-frm").serialize()+'&action=login',

                success: function(response)
                {	
                    if(response === 'ok') {
                        window.location = 'dashboard.php';
                    } else {
                        $("#alert").show();
                        $("#result").html(response);
                    }
                }
            });
        }
        return true;
    })

    // for forget Password
    $("#reset").click(function(e) {
        if(document.getElementById('forgot-frm').checkValidity()) {
            e.preventDefault();
            $.ajax({
                url: 'data/register-form.php',
                method: 'POST',
                data:$("#forgot-frm").serialize()+'&action=reset',

                success: function(response)
                {	
                    $("#alert").show();
                    $("#result").html(response);
                }
            });
        }
        return true;
    })

});