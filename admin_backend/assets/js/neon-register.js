/**
 *	Neon Register Script
 *
 *	Developed by Arlind Nushi - www.laborator.co
 */

var neonRegister = neonRegister || {};

(function($, window, undefined)
{
	"use strict";
	
	$(document).ready(function()
	{
		neonRegister.$container = $("#form_register");		
		
		neonRegister.$container.validate({
			rules: {
				name: {
					required: true
				},
				
				email: {
					required: true,
					email: true
				},
				
				username: {
					required: true	
				},
				
				password: {
					required: true
				},

				passwordtwo: {
					required: true
				},

				passwordtwo: {
					equalTo: "#password"
				},
				
			},
			
			messages: {
				
				email: {
					email: 'Invalid E-mail.'
				}	
			},
			
			highlight: function(element){
				$(element).closest('.input-group').addClass('validate-has-error');
			},
			
			
			unhighlight: function(element)
			{
				$(element).closest('.input-group').removeClass('validate-has-error');
			},
		});	

		$("#register-btn").click(function(e) {
			if(document.getElementById('form-register').checkValidity()) {
				e.preventDefault();
				$.ajax({
					url: baseurl + 'data/register-form.php',
					method: 'POST',
					data:$("#form_register").serialize()+'&action=register',

					success: function(response)
					{	
						$("#register-success").show();
						$("#result").html(response);
					}
				});
			}
			return true;
		})
		
	});
	
})(jQuery, window);