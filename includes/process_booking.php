<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

     require '../admin_backend/data/config.php';

     if(isset($_POST['action']) && $_POST['action'] == 'book') {


          // Fields Submitted
          $name       	               = check_input($_POST['name']);
          $phone   		               = check_input($_POST['phone']);
          $email      	               = check_input($_POST['email']);
          $from_address   		     = check_input($_POST['from_address']);
          $from_state   		          = check_input($_POST['from_state']);
          $to_address   		          = check_input($_POST['to_address']);
          $to_state   		          = check_input($_POST['to_state']);
          $from_type   		          = check_input($_POST['from_type']);
          $from_beds   		          = check_input($_POST['from_beds']);
          $from_floor   		          = check_input($_POST['from_floor']);
          $to_type   		          = check_input($_POST['to_type']);
          $to_beds   		          = check_input($_POST['to_beds']);
          $to_floor   		          = check_input($_POST['to_floor']);
          $additional_item   		     = check_input($_POST['additional_item']);
          $services   		          = $_POST['services'];
          $move_date   		          = check_input($_POST['move_date']);
          $referral_code   		     = check_input($_POST['referral_code']);
          $special_instruction   		= check_input($_POST['special_instruction']);
          $created		               = date('Y-m-d');

          $item_quantities              = $_POST['itemQuantities'];
          $item_ids                     = $_POST['itemIds'];
          $section_ids                  = $_POST['sectionIds'];

          $required_services = implode(', ', $services);
     
          $book = $conn->prepare("INSERT INTO moving_entries (
                                                            full_name,
                                                            phone, 
                                                            email, 
                                                            from_address, 
                                                            from_state, 
                                                            to_address, 
                                                            to_state, 
                                                            from_type, 
                                                            from_beds, 
                                                            from_floor, 
                                                            to_type, 
                                                            to_beds, 
                                                            to_floor, 
                                                            additional_items, 
                                                            required_services, 
                                                            move_date, 
                                                            referral_code, 
                                                            special_instruction, 
                                                            created
                                   ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
          $book->bind_param(
               "ssssssssiisiissssss", 
               $name, 
               $phone, 
               $email, 
               $from_address, 
               $from_state, 
               $to_address, 
               $to_state, 
               $from_type, 
               $from_beds, 
               $from_floor, 
               $to_type, 
               $to_beds, 
               $to_floor, 
               $additional_item, 
               $required_services, 
               $move_date, 
               $referral_code, 
               $special_instruction, 
               $created
          );
          
          if($book->execute()) {

               $run = $conn->prepare("INSERT INTO room_entries(moving_id, section_id, item_id, item_qty) VALUES((SELECT MAX(moving_id) FROM moving_entries), ?, ?, ?)");
               foreach($item_quantities as $key => $value) {
                    $run->bind_param("iii", $section_ids[$key], $item_ids[$key], $value);
                    //echo $value . PHP_EOL . $item_ids[$key] . PHP_EOL . $section_ids[$key];
                    $run->execute();
               }
               echo $name;

          } else {
               trigger_error("there was an error....".$conn->error, E_USER_WARNING);
          }     
     }


     function check_input($data) {
          $data = trim($data);
          $data = stripslashes($data);
          $data = htmlspecialchars($data);
          return $data;
     }