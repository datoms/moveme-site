/*
| ==========================================================
| Preloader
| ========================================================== */
jQuery(window).load(function() {
    $('#preloader').fadeOut('50');
});

// INCLUDE JQUERY & JQUERY UI 1.12.1
$(function() {
    $("#datepicker").datepicker({
        dateFormat: "dd-mm-yy",
        duration: "fast"
    });
});



/*
| ==========================================================
| Scroll To Top
| ========================================================== */

$(document).ready(function() {
    'use strict';
    // Scroll To Top
    $('body').prepend('<div class="go-top"><span id="top"><img src="assets/img/scroll-to-top.svg" alt="top" /></span></div>');

    $(window).scroll(function() {
        if ($(window).scrollTop() > 500) {
            $('.go-top').fadeIn(600);
        } else {
            $('.go-top').fadeOut(600);
        }
    });
    $('#top').click(function() {
        $('html, body').animate({ scrollTop: 0 }, 800, 'easeInQuad');
        return false;
    });


});


/*
| ==========================================================
| 
| ========================================================== */

$(document).ready(function() {

    'use strict';
    $(function() {
        //caches a jQuery object containing the header element
        var header = $(".desk-nav");
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();

            if (scroll >= 30) {
                header.addClass("darkHeader");
            } else {
                header.removeClass("darkHeader");
            }
        });
    });
    // Tab active class
    $('.goto-tab-1').click(function() {
        $('.nav.nav-tabs.nav-tab-parent li a').removeClass('active show');
        $('.nav.nav-tabs.nav-tab-parent li:nth-child(1) a').addClass('active show');
    });
    $('.goto-tab-2').click(function() {
        $('.nav.nav-tabs.nav-tab-parent li a').removeClass('active show');
        $('.nav.nav-tabs.nav-tab-parent li:nth-child(2) a').addClass('active show');
    });
    $('.goto-tab-3').click(function() {
        $('.nav.nav-tabs.nav-tab-parent li a').removeClass('active show');
        $('.nav.nav-tabs.nav-tab-parent li:nth-child(3) a').addClass('active show');
    });

    $('.goto-tab-4').click(function() {
        $('.nav.nav-tabs.nav-tab-parent li a').removeClass('active show');
        $('.nav.nav-tabs.nav-tab-parent li:nth-child(4) a').addClass('active show');
    });

    // $('.nav.nav-tabs.nav-tab-parent li a').click(function() {
    //     $('.nav.nav-tabs.nav-tab-parent li a').removeClass('active show');
    //     $(this).addClass('active show');
    // });
    // Logo slider
    $('.customer-logos').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        // autoplay: true,
        autoplaySpeed: 1500,
        arrows: true,
        dots: false,
        margin: 10,
        pauseOnHover: false,
        nextArrow: '<div class="slick-right"><img src="assets/images/arrow.png" alt="" /></div>',
        prevArrow: '<div class="slick-left"><img src="assets/images/arrow-back.png" alt="" /></div>',
        responsive: [{
            breakpoint: 1199,
            settings: {
                slidesToShow: 3
            }
        }, {
            breakpoint: 991,
            settings: {
                slidesToShow: 3
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 570,
            settings: {
                slidesToShow: 1
            }
        }]
    });
    // Logo slider
    $('.test-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        // autoplay: true,
        autoplaySpeed: 1500,
        arrows: true,
        dots: false,
        margin: 10,
        pauseOnHover: false,
        nextArrow: '<div class="slick-right"><img src="assets/images/arrow.png" alt="" /></div>',
        prevArrow: '<div class="slick-left"><img src="assets/images/arrow-back.png" alt="" /></div>',
        responsive: [{
            breakpoint: 1199,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 1
            }
        }]
    });
    // Logo slider
    $('.logos-slider').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        // autoplay: true,
        autoplaySpeed: 1500,
        arrows: true,
        dots: false,
        margin: 10,
        pauseOnHover: false,
        nextArrow: '<div class="slick-right"><img src="assets/images/arrow.png" alt="" /></div>',
        prevArrow: '<div class="slick-left"><img src="assets/images/arrow-back.png" alt="" /></div>',
        responsive: [{
            breakpoint: 1600,
            settings: {
                slidesToShow: 4
            }
        }, {
            breakpoint: 1199,
            settings: {
                slidesToShow: 3
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 400,
            settings: {
                slidesToShow: 1
            }
        }]
    });
    // Match Height
    $('.test-slider-area').each(function() {
        $(this).find('.test-slider-inner').matchHeight();
    });
    // Match Height
    $('.our-process-area').each(function() {
        $(this).find('.single-process-img').matchHeight();
    });
});



/*
| ==========================================================
| Mobile Menu
| ========================================================== */

$(document).ready(function() {
    $('.nex-menu').nexmenu({
        nexBarPosition: "right", // left right 
        nexMenuBg: "#f80546",
        brandLogo: "<img src='assets/images/diggywhite.png'/>",
        nexBarColor: "white",
        nexMenuPosition: "fixed",
        brandPosition: "left",
        nexScreenWidth: "991",
        nexShowChildren: true,
        nexExpandableChildren: true,
        nexExpand: "+",
        nexContract: "-",
        nexRemoveAttrs: true,
        onePage: true,
        removeElements: ".desk-nav"
    });

});





/*
| ==========================================================
| Onepage Nav
| ========================================================== */
$(document).scroll(function() {
    $('.desk-nav, .nex-menu').onePageNav({
        currentClass: 'active',
        changeHash: true,
        scrollSpeed: 900,
        scrollOffset: 0,
        scrollThreshold: 0.3,
        filter: ':not(.no-scroll)'
    });

});