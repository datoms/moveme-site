jQuery(function () {
    "use strict";

    /*global jQuery, $*/
    // jQuery(document).ready(function () {
    //
    //
    //     // Parallax
    //     $('.home-area').parallax("50%", 0.1);
    //     $('.cta-area').parallax("50%", 0.1);
    //     $('.testimonial-innr').parallax("50%", 0.1);
    //     $('.contact-info-area').parallax("50%", 0.1);
    //
        // OWL Carousel
        // $("#owl-example").owlCarousel({
        //
        //     autoPlay: 3000, //Set AutoPlay to 3 seconds
        //     singleItem: true
        //
        // });
    //
    //     // go-to-form
    //     jQuery(window).bind('scroll', function (e) {
    //         parallax();
    //     });
    //
    //
    //     jQuery('.more-feature').on('click', function () {
    //         jQuery('html, body').animate({scrollTop: $('#more-feature').offset().top - 0}, 1500,
    //             function () {
    //                 parallax();
    //             });
    //         return false;
    //     });
    //
    //     jQuery('.go-form').on('click', function () {
    //         jQuery('html, body').animate({scrollTop: $('#form-area').offset().top - 0}, 1500,
    //             function () {
    //                 parallax();
    //             });
    //         return false;
    //     });
    //
    //     function parallax() {
    //         var scrollPosition = $(window).scrollTop();
    //     }
    //
    // });
$(document).ready(function () {
    $('#specialToggleButton').click(function () {
        if ($(this).find("b").html() == "Add") {
            $('#specialTextArea').slideToggle();
            $(this).find("b").html("Hide");
            $(this).find("span").addClass("fa-minus").removeClass("fa-plus");
        } else {
            $('#specialTextArea').slideToggle();
            $(this).find("b").html("Add");
            $(this).find("span").addClass("fa-plus").removeClass("fa-minus");
        }
    });

    $('#nextBtn').click(function () {
        var currentlyActive = $('.nav-tabs-one').find(".active");
        currentlyActive.next().find("a").tab('show');
    });

    $('#prevBtn').click(function () {
        var currentlyActive = $('.nav-tabs-one').find(".active");
        currentlyActive.prev().find("a").tab('show');
    });

    $('.dropdown-toggle').dropdown();

});

});
