$('document').ready(function () {
    var addMoreDropDown = $('.add-more');
    addMoreDropDown.change(
        function () {
            var selectedOption = $(this).find('option:selected');

            var itemId = selectedOption.val();

            // ie if the first <option> is selected
            if (itemId === 'no') {
                return;
            }

            var itemSlug = selectedOption.data('slug');
            var itemSection = selectedOption.data('section');
            var itemName = selectedOption.data('name');

            var slugSection = itemSlug + itemSection;
            var id = "id-" + slugSection;
            var msId = "ms-" + slugSection;
            var psId = "ps-" + slugSection;

            var appendRow =
                " <tr id=" + id + ">" +
                " <td class='timesTData text-center'  style='vertical-align: middle;'" +
                " width='15%'><span class='opening' style='visibility: hidden;'>(</span><span class='digit' style='visibility: hidden;'>1</span><span" +
                " class='fa fa-times' style='visibility: hidden;'></span><span class='closing' style='visibility: hidden;'>)</span></td>" +
                " <td class='nameTData' width='55%' style='vertical-align: middle'>" + itemName + "</td>" +
                " <td class='minusTData text-center' style='vertical-align: middle;' width='10%'" +
                " id='" + msId + "' data-id='" + id + "'><span class='fa fa-minus fa-lg' style='visibility: hidden;'></span></td>" +
                " <td class='plusTData text-center' style='vertical-align: middle;' width='10%'" +
                " id='" + psId + "' data-id='" + id + "'><span class='fa fa-plus fa-lg' style='visibility: hidden;'></span></td>" +
                " <td class='checkTData' width='10%'><input value='1' type='checkbox' class='form-control checkBox " + slugSection + "'" +
                " name='itemQuantities[]' title='" + itemName + "' data-href='" + slugSection + "'" +
                " data-tr='" + id + "'><input type='checkbox' name='itemIds[]' class='" + slugSection + "'" +
                " value='" + itemId + "' style='display: none;'><input type='checkbox' name='sectionIds[]'" +
                " class='" + slugSection + "' value='" + itemSection + "' style='display: none;'>" +
                " </td>" +
                " </tr>";

            if ($("#" + id).length === 1) {
                // i.e. we've added it b4, so don't do anything
            } else {
                $(this).parentsUntil('div.tabpanel', 'div.form-group').prev().find('table tbody').append(appendRow);
            }
            // alert(selectedOption.val() + ' ' + selectedOption.text());
        }
    );


    $('tbody').on('change', '.checkBox', function (event) {
        //in order to  be able to reference the parent <tr> element. 'tdId' is a misnomer
        var tdId = $(this).attr('data-tr');

        if ($(event.target).is(':checked')) {
            $(this).siblings().prop("checked", true);

            var boxId = $(this).attr('data-href');

            makeVisible(tdId); // to make the + and - used for incrementing/decrementing visible

            var temp = parseInt($('#' + tdId).find('.timesTData .digit').text());

            // we want to attach click event for decrementing to the minus span
            var ms = "ms-" + boxId;
            $('#' + ms).click(function () {
                if (temp == 1) {
                    // used to hide the plus and minus and to uncheck d main checkbox and its siblings
                    hideAndUncheck(tdId);
                    return false;
                }
                temp--;
                $('#' + tdId).find(".timesTData .digit").text(temp);
                $('#' + tdId).find(".checkTData .checkBox").val(temp);
            });
            // we want to attach click event for incrementing to the plus span
            var ps = "ps-" + boxId;
            $('#' + ps).click(function () {
                temp++;
                $('#' + tdId).find(".timesTData .digit").text(temp);
                $('#' + tdId).find(".checkTData .checkBox").val(temp);
            });

        } else if ($(event.target).not(':checked')) {
            //this fn just hides the x, + and - and set digit=1
            checkBoxUnclicked(tdId);
            $(this).siblings().prop("checked", false);
        }
    });

    function hideAndUncheck(parentId) {
        var parentTr = $("#" + parentId);
        parentTr.find(".timesTData .digit").text(1);
        parentTr.find(".timesTData span").css("visibility", "hidden");
        parentTr.find(".minusTData span").css("visibility", "hidden");
        parentTr.find(".plusTData span").css("visibility", "hidden");
        parentTr.find(".checkTData .checkBox").attr("checked", false);
        parentTr.find(".checkTData .checkBox").siblings().attr("checked", false);
    }

    function makeVisible(parentId) {
        var parentTr = $("#" + parentId);
        parentTr.find('.timesTData span').css("visibility", "visible");
        parentTr.find('.minusTData span').css("visibility", "visible");
        parentTr.find('.plusTData span').css("visibility", "visible");
        // parentTr.find('.plusTData span').css("border-left", "thin solid black");
    }

    function checkBoxUnclicked(parentId) {
        var parentTr = $("#" + parentId);
        parentTr.find(".timesTData .digit").text(1);
        parentTr.find(".timesTData span").css("visibility", "hidden");
        parentTr.find(".minusTData span").css("visibility", "hidden");
        parentTr.find(".plusTData span").css("visibility", "hidden");
    }

    try {
        $('#move-date').datepicker({
            dateFormat: "yy-mm-dd",
            minDate: 0
        });
    } catch (e) {

    }

    // $('.main-submit').click(function () {
    //     if (_validateInputs()) {
    //         $('#form_list').submit();
    //     } else {
    //         alert('Please check all sections to fix the errors.');
    //     }
    // });

    $('.main-submit').click(function (e) {
        if (_validateInputs()) {
            e.preventDefault();

            $.ajax({
                type: "POST",
                url: "../../../includes/process_booking.php",
                data: $("#form_list").serialize() + '&action=book',
                success: function (response) {
                    $('.last').hide();
                    $('.finish').show();
                    $('.prog').addClass('active');
                    $('prog-bar .prog.active::after').css("right", "0%");
                    $("#get-name").html(response);
                    //$("#result").html(response);
                }
            });

            //$('#form_list').submit();
        } else {
            alert('Please check all sections to fix the errors.');
        }
    });

    // $('.main-submit').click(function (e) {
    //     if (_validateInputs()) {
    //         e.preventDefault();

    //         $.ajax({
    //             type: "POST",
    //             url: "../../../includes/process_booking.php",
    //             data: $("#form_list").serialize() + '&action=book',
    //             success: function (response) {
    //                 // $('.last').hide();
    //                 // $('.finish').show();
    //                 // $('.prog').addClass('active');
    //                 // $('prog-bar .prog.active::after').css("right", "0%");
    //                 $("#result").html(response);
    //             }
    //         });

    //         //$('#form_list').submit();
    //     } else {
    //         alert('Please check all sections to fix the errors.');
    //     }
    // });


    // $("#main-submit").click(function (e) {
    //     e.preventDefault();
    //     $.ajax({
    //         type: "POST",
    //         url: "../../../includes/process_booking.php",
    //         data: $("#form_list").serialize() + '&action=book',
    //         success: function (response) {
    //             $("#result").html(response);
    //         }
    //     });
    //     return true;
    // });


    function _validateInputs() {
        var _valid = true;
        var _allRequiredInputs = $('#form_list input[required]');

        var _filteredRequiredInputs = _allRequiredInputs.not(function () {
            if ($(this.parentElement).hasClass('field_sets')) {
                return true;
            }
        });

        var _addressInputs = _allRequiredInputs.map(function () {
            if ($(this.parentElement).hasClass('field_sets')) {
                return this;
            }
        });

        var nameInput = _filteredRequiredInputs.eq(0);
        var phoneInput = _filteredRequiredInputs.eq(1);
        var emailInput = _filteredRequiredInputs.eq(2);
        var dateInput = _filteredRequiredInputs.eq(3);


        var trimmedName = $.trim(nameInput.val());
        if (trimmedName.length === 0) {
            appendRequiredSpan(nameInput);
        } else if (trimmedName.length < 3) {
            _valid = false;
            if (isErrorSpanAfter(nameInput)) {
                nameInput.next().remove();
            }
            $('<span class="error-span">MUST BE MINIMUM OF 3 CHARACTERS</span>').insertAfter(nameInput);
        } else {
            if (isErrorSpanAfter(nameInput)) {
                nameInput.next().remove();
            }
        }


        var trimmedPhone = $.trim(phoneInput.val());
        if (trimmedPhone.length === 0) {
            appendRequiredSpan(phoneInput);
        } else if (trimmedPhone.match(/^0\d{10}$/) === null) {
            _valid = false;
            if (isErrorSpanAfter(phoneInput)) {
                phoneInput.next().remove();
            }
            $('<span class="error-span">INVALID PHONE FORMAT</span>').insertAfter(phoneInput);
        } else {
            if (isErrorSpanAfter(phoneInput)) {
                phoneInput.next().remove();
            }
        }


        var trimmedEmail = $.trim(emailInput.val());
        if (trimmedEmail.length === 0) {
            appendRequiredSpan(emailInput);
        } else if (trimmedEmail.toLowerCase().match(/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/) === null) {
            _valid = false;
            if (isErrorSpanAfter(emailInput)) {
                emailInput.next().remove();
            }
            $('<span class="error-span">INVALID EMAIL FORMAT</span>').insertAfter(emailInput);
        } else {
            if (isErrorSpanAfter(emailInput)) {
                emailInput.next().remove();
            }
        }


        var trimmedDate = $.trim(dateInput.val());
        if (trimmedDate.length === 0) {
            appendRequiredSpan(dateInput);
        } else if (trimmedDate.match(/^20\d{2}-\d{2}-\d{2}$/) === null) {
            _valid = false;
            if (isErrorSpanAfter(dateInput)) {
                dateInput.next().remove();
            }
            $('<span class="error-span">INVALID DATE FORMAT</span>').insertAfter(dateInput);
        }
        /*else if ( (new Date(trimmedDate)).toDateString() !== (new Date()).toDateString() )
        {
            _valid = false;
            if (isErrorSpanAfter(dateInput)) {
                dateInput.next().remove();
            }
            $('<span class="error-span">DATE CANNOT BE LESS THAN TODAY</span>').insertAfter( dateInput );
        }*/
        else {
            if (isErrorSpanAfter(dateInput)) {
                dateInput.next().remove();
            }
        }



        var fromAddrInput = _addressInputs.eq(0);
        var toAddrInput = _addressInputs.eq(1);

        var isFromAddressValid = true;
        var isToAddressValid = true;


        var trimmedAddr = $.trim(fromAddrInput.val());
        if (trimmedAddr.length === 0) {
            appendRequiredSpanAfterNext(fromAddrInput);
            isFromAddressValid = false;
        } else if (trimmedAddr.length < 3) {
            _valid = false;
            isFromAddressValid = false;
            if (isErrorSpanAfterNext(fromAddrInput)) {
                fromAddrInput.next().next().remove();
            }
            $('<span class="error-span">MUST BE MINIMUM OF 3 CHARACTERS</span>').insertAfter(fromAddrInput.next());
        } else {
            if (isErrorSpanAfterNext(fromAddrInput)) {
                fromAddrInput.next().next().remove();
            }
        }


        trimmedAddr = $.trim(toAddrInput.val());
        if (trimmedAddr.length === 0) {
            appendRequiredSpanAfterNext(toAddrInput);
            isToAddressValid = false;
        } else if (trimmedAddr.length < 3) {
            _valid = false;
            isToAddressValid = false;
            if (isErrorSpanAfterNext(toAddrInput)) {
                toAddrInput.next().next().remove();
            }
            $('<span class="error-span">MUST BE MINIMUM OF 3 CHARACTERS</span>').insertAfter(toAddrInput.next());
        } else {
            if (isErrorSpanAfterNext(toAddrInput)) {
                toAddrInput.next().next().remove();
            }
        }


        var requiredDropdowns = $('select[required]');
        var fromStateDropdown = requiredDropdowns.eq(0);
        var toStateDropdown = requiredDropdowns.eq(1);

        if (isFromAddressValid) {
            //only try to validate if there's no issue already due to the address field above
            var grandParent = fromStateDropdown.parent().parent();
            var erSp = grandParent.find('span.error-span');
            if (fromStateDropdown.get(0).selectedIndex === 0) {
                _valid = false;
                //only append if there's none there already
                if (!erSp.length) {
                    grandParent.append('<span class="error-span">REQUIRED</span>');
                }
            } else {
                if (erSp.length) {
                    erSp.remove();
                }
            }
        }

        if (isToAddressValid) {
            //only try to validate if there's no issue already due to the address field above
            var grandParent = toStateDropdown.parent().parent();
            var erSp = grandParent.find('span.error-span');
            if (toStateDropdown.get(0).selectedIndex === 0) {
                _valid = false;
                //only append if there's none there already
                if (!erSp.length) {
                    grandParent.append('<span class="error-span">REQUIRED</span>');
                }
            } else {
                if (erSp.length) {
                    erSp.remove();
                }
            }
        }



        var fromTypeDropdown = requiredDropdowns.eq(2);
        var fromBedDropdown = requiredDropdowns.eq(3);
        var fromFloorDropdown = requiredDropdowns.eq(4); {
            var grandParent = fromTypeDropdown.parent().parent();
            var erSp = grandParent.find('span.error-span');

            if (fromTypeDropdown.get(0).selectedIndex === 0 || fromBedDropdown.get(0).selectedIndex === 0 || fromFloorDropdown.get(0).selectedIndex === 0) {
                _valid = false;
                if (!erSp.length) {
                    //only append if there's none there already
                    grandParent.append('<span class="error-span">REQUIRED</span>');
                }
            } else {
                if (erSp.length) {
                    erSp.remove();
                }
            }
        }


        var toTypeDropdown = requiredDropdowns.eq(5);
        var toBedDropdown = requiredDropdowns.eq(6);
        var toFloorDropdown = requiredDropdowns.eq(7); {
            var grandParent = toTypeDropdown.parent().parent();
            var erSp = grandParent.find('span.error-span');

            if (toTypeDropdown.get(0).selectedIndex === 0 || toBedDropdown.get(0).selectedIndex === 0 || toFloorDropdown.get(0).selectedIndex === 0) {
                _valid = false;
                if (!erSp.length) {
                    //only append if there's none there already
                    grandParent.append('<span class="error-span">REQUIRED</span>');
                }
            } else {
                if (erSp.length) {
                    erSp.remove();
                }
            }
        }

        if (!_valid) {
            //if there's already an error-span due to b-end validation,then we don't want to have double
            var _errSpan = $('input[name="name"]').parent().parent().prev().not('span[id]');
            if (_errSpan.length) {
                //don't do anything
            } else {
                $('#front-validator').show();
            }
        } else {
            $('#front-validator').hide();
        }


        return _valid;
    }

    function isErrorSpanAfter(ws) {
        var errorSpan = ws.next();
        if (errorSpan && errorSpan.hasClass('error-span')) {
            return true;
        }
        //else
        return false;
    }

    function isErrorSpanAfterNext(ws) {
        var errorSpan = ws.next().next();
        if (errorSpan && errorSpan.hasClass('error-span')) {
            return true;
        }
        //else
        return false;
    }

    function appendRequiredSpan(ws) {
        _valid = false;
        if (isErrorSpanAfter(ws)) {
            ws.next().remove();
        }
        $('<span class="error-span">REQUIRED</span>').insertAfter(ws);
    }

    function appendRequiredSpanAfterNext(ws) {
        _valid = false;
        if (isErrorSpanAfterNext(ws)) {
            ws.next().next().remove();
        }
        $('<span class="error-span">REQUIRED</span>').insertAfter(ws.next());
    }

    var movesCounterElement = document.getElementById('movesCounter');
    var clientsCounterElement = document.getElementById('clientsCounter');
    var goodsCounterElement = document.getElementById('goodsCounter');
    var weightCounterElement = document.getElementById('weightCounter');

    // rmbr that these elements above live on the homepage only
    // suffices to check with one of them since they all are on the same page
    if (!isNullOrUndefined(movesCounterElement)) {
        // set this cos we want the counting to be done once only
        // although it doesn't recount, but just to be sure :)
        var _movesDone = false;
        var _clientsDone = false;
        var _goodsDone = false;
        var _weightDone = false;

        var options = {
            useEasing: true,
            useGrouping: true,
            separator: ',',
            decimal: '.',
        };

        var _movesCountUp = new CountUp("movesCounter", 0, totalMoves, 0, 2.5, options);
        var _clientsCountUp = new CountUp("clientsCounter", 0, totalClients, 0, 2.5, options);
        var _goodsCountUp = new CountUp("goodsCounter", 0, totalGoods, 0, 2.5, options);
        var _weightCountUp = new CountUp("weightCounter", 0, totalWeight, 0, 2.5, options);

        // if it's already visible e.g. user just did a refresh while it's visible
        countMovesNow();
        countClientsNow();
        countGoodsNow();
        countWeightNow();

        // else, wait till user scrolls
        $(window).scroll(function () {
            countMovesNow();
            countClientsNow();
            countGoodsNow();
            countWeightNow();
        });
    }

    function isNullOrUndefined(variable) {
        if (typeof variable === 'undefined' || variable === null) {
            // variable is undefined or null
            return true;
        } else {
            return false;
        }
    }

    function isScrolledIntoView(elem) {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();

        var elemTop = $(elem).offset().top;
        var elemBottom = elemTop + $(elem).height();

        return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }

    function countMovesNow() {
        if (isScrolledIntoView(movesCounterElement) && !_movesDone) {
            _movesDone = true;
            setTimeout(function () {
                _movesCountUp.start();
            }, 600);
        }
    }

    function countClientsNow() {
        if (isScrolledIntoView(clientsCounterElement) && !_clientsDone) {
            _clientsDone = true;
            setTimeout(function () {
                _clientsCountUp.start();
            }, 600);
        }
    }

    function countGoodsNow() {
        if (isScrolledIntoView(goodsCounterElement) && !_goodsDone) {
            _goodsDone = true;
            setTimeout(function () {
                _goodsCountUp.start();
            }, 600);
        }
    }

    function countWeightNow() {
        if (isScrolledIntoView(weightCounterElement) && !_weightDone) {
            _weightDone = true;
            setTimeout(function () {
                _weightCountUp.start();
            }, 600);
        }
    }

    $('#phone-span').click(function () {
        $('#show-phone').modal();
    });

});