if($('#form_list').length > 0){
    $('#carousel-example-generic .item.active .carousel-control').live('click',function(event){
        clicked=$(this);
        if(clicked.attr('data-slide')=="next"){
        next=(parseInt(clicked.attr('data-count')))+1
            $('.prog-bar .process_'+next).addClass('active');
        }else{
            next=(parseInt(clicked.attr('data-count')))
            $('.prog-bar .process_'+next).removeClass('active');
        }
    })
}

$('document').ready(function(){
    $("select").selectOrDie();

    if($('.loop').length > 0){
        $('.loop').owlCarousel({
            center: true,
            items:2,
            loop:true,
            nav:true,
            margin:10,
            responsive:{
                600:{
                    items:3
                }
            }
        });
    }

    if($('#form_list').length > 0){
        $('.nav.nav-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $('.item.active .tab_list').height($('.tab_content_list').height())
        });

        $('#carousel-example-generic').on('slid.bs.carousel', function (event) {
            $('.item.active .tab_list').height($('.tab_content_list').height())
        })
    }
});
